#
curl --socks5-hostname 127.0.0.1:7073 https://commondatastorage.googleapis.com/chromium-browser-official/chromium-83.0.4103.116.tar.xz

docker run --name keys --entrypoint abuild-keygen -e PACKAGER="Yonggang Luo <luoyonggang@gmail.com>" andyshinn/alpine-abuild:v10 -n

mkdir ~/.abuild
docker cp keys:/home/builder/.abuild/luoyonggang@gmail.com-5f0a6ce3.rsa ~/.abuild/abuild-key.rsa
docker cp keys:/home/builder/.abuild/luoyonggang@gmail.com-5f0a6ce3.rsa.pub ~/.abuild/abuild-key.rsa.pub

docker pull andyshinn/alpine-abuild:v11
cd ~/shared/work/docker-alpine-abuild/
docker build . --tag andyshinn/alpine-abuild:v11
docker save andyshinn/alpine-abuild:v11 -o $HOME/shared/dockers/alpine-abuild-v11.tar
docker load -i $HOME/shared/dockers/alpine-abuild-v11.tar

docker run \
	-e RSA_PRIVATE_KEY="$(cat ~/.abuild/abuild-key.rsa)" \
	-e RSA_PRIVATE_KEY_NAME="abuild-key.rsa" \
	-v "$PWD:/home/builder/package" \
	-v "$HOME/shared/.abuild/packages:/packages" \
	-v "$HOME/.abuild/abuild-key.rsa.pub:/etc/apk/keys/abuild-key.rsa.pub" \
	andyshinn/alpine-abuild:v11
# Building anbox
cd ~/shared/work/aports/testing/anbox
echo '/tmp/core.%t.%e.%p' | sudo tee /proc/sys/kernel/core_pattern
docker run -it --name anbox --rm \
	-e RSA_PRIVATE_KEY="$(cat ~/.abuild/abuild-key.rsa)" \
	-e RSA_PRIVATE_KEY_NAME="abuild-key.rsa" \
	-v /etc/apk/repositories12:/etc/apk/repositories \
	-v "$PWD:/home/builder/package" \
	-v "$HOME/shared/.abuild/packages:/packages" \
	-v "$HOME/.abuild/abuild-key.rsa.pub:/etc/apk/keys/abuild-key.rsa.pub" \
	--entrypoint "" \
	--ulimit core=-1 --security-opt seccomp=unconfined \
	andyshinn/alpine-abuild:v11 sh
sudo sudo chown builder:abuild -R .
abuild deps
abuild unpack
abuild prepare
sudo apk add properties-cpp-dev@edge cmake-extras@edge
abuild build
rm -rf pkg
abuild package
abuilder rootpkg

After build, install on host
BUILDER=~/shared/.abuild/packages/builder/x86_64
apk add --no-cache --allow-untrusted $BUILDER/anbox-openrc-0_git20200609-r1.apk $BUILDER/anbox-0_git20200609-r1.apk

# Building chrome
cd ~/shared/work/aports/community/chromium
# Enable coredump
echo '/tmp/core.%t.%e.%p' | sudo tee /proc/sys/kernel/core_pattern
docker run -it --name chrome --rm \
	-e RSA_PRIVATE_KEY="$(cat ~/.abuild/abuild-key.rsa)" \
	-e RSA_PRIVATE_KEY_NAME="abuild-key.rsa" \
	-v /etc/apk/repositories12:/etc/apk/repositories \
	-v "$PWD:/home/builder/package" \
	-v "$HOME/shared/.abuild/packages:/packages" \
	-v "$HOME/.abuild/abuild-key.rsa.pub:/etc/apk/keys/abuild-key.rsa.pub" \
	--entrypoint "" \
	--ulimit core=-1 --security-opt seccomp=unconfined \
	andyshinn/alpine-abuild:v11 sh

sudo sudo chown builder:abuild -R .

docker load -i ~/shared/work/chrome-alpine12.tar
cd ~/shared/work/aports/community/chromium
docker run -it --name chrome \
	-e RSA_PRIVATE_KEY="$(cat ~/.abuild/abuild-key.rsa)" \
	-e RSA_PRIVATE_KEY_NAME="abuild-key.rsa" \
	-v /etc/apk/repositories12:/etc/apk/repositories \
	-v "$PWD:/home/builder/package" \
	-v "$HOME/shared/.abuild/packages:/packages" \
	-v "$HOME/.abuild/abuild-key.rsa.pub:/etc/apk/keys/abuild-key.rsa.pub" \
	--entrypoint "" \
	--ulimit core=-1 --security-opt seccomp=unconfined \
	chrome-alpine12 sh

sudo ./muslstack -s 0x800000 /usr/bin/clang
sudo ./muslstack -s 0x800000 /usr/bin/lld
sudo ./muslstack -s 0x800000 /usr/bin/lli
sudo ./muslstack -s 0x800000 /usr/bin/llc


git config --global credential.helper store
git config --global user.email "luoyonggang@gmail.com"
git config --global user.name "Yonggang Luo"

# Building procedure

abuilder -help
abuild cleanup
abuild deps
abuild deps_other
abuild unpack
abuild prepare_git
abuild prepare_patches
# Git format patches
git format-patch -5


abuild prepare
abuild build
rm -rf /home/builder/package/pkg
abuild package
abuilder rootpkg

# cmake -DCMAKE_BUILD_TYPE=Debug -DLLVM_ENABLE_PROJECTS=lld -DCMAKE_INSTALL_PREFIX=/usr/local -G Ninja ../llvm-project/llvm